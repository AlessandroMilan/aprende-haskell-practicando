
intersperse :: a -> [[a]] -> [a]

intersperse a (x:xs@(_:_)) = x ++ a:intersperse a xs
intersperse a (x:_) = x
intersperse _ _ = []
