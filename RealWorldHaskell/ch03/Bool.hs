import Prelude hiding (Bool(..))


data Bool = False | True

