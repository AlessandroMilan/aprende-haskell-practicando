import Data.List

type Edge = (String, STree)

data STree = Node [Edge]
           | Leaf
             deriving (Show)

type EdgeFunction = [String] -> (Int, [String])

construct :: String -> STree
construct = suf . suffixes
    where suf [[]] = Leaf
          suf ss = Node [([a], suf n)
                         | a <- ['\0'..'\255'],
                           n@(sa:_) <- [ss `clusterBy` a]]
          clusterBy ss a = [cs | c:cs <- ss, c == a]

construct2 :: EdgeFunction -> [Char] -> String -> STree
construct2 edge alphabet = suf . suffixes
    where suf [[]] = Leaf
          suf ss = Node [(take (cpl+1) (a:sa), suf ssr)
                         | a <- alphabet,
                           n@(sa:_) <- [ss `clusterBy` a],
                           (cpl,ssr) <- [edge n]]
          clusterBy ss a = [cs | c:cs <- ss, c == a]

simple :: EdgeFunction
simple n = (0, n)

cst :: EdgeFunction
cst [s] = (length s, [[]])
cst awss@((a:w):ss)
    | null [c | c:_ <- ss, a /= c] = (cpl + 1, rss)
    | otherwise = (0, awss)
    where (cpl, rss) = cst (w:[u | _:u <- ss])


suffixes :: [a] -> [[a]]
suffixes xs@(_:xs') = xs : suffixes xs'
suffixes _ = []
noAsPattern :: [a] -> [[a]]
noAsPattern (x:xs) = (x:xs) : noAsPattern xs
noAsPattern _ = []                 

suffixes2 xs = init (tails xs)

compose :: (b -> c) -> (a -> b) -> a -> c
compose f g x = f (g x)

suffixes3 xs = compose init tails xs

suffixes4 = compose init tails

suffixes5 = init . tails
