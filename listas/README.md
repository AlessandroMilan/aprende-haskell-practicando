## Listas

En Haskell las listas se encierran entre corchetes, cada elemento de la lista se incluye dentro de los mismos separado por coma. 

```bash
ghci> [1, 2, 3]
[1,2,3]
```

Las listas pueden ser de cualquier tamaño, incluyendo listas sin elementos.

```bash
ghci> []
[]
```

Es indispensable que todos los elementos dentro de las listas sean del mismo tipo, incluyendo listas sin elementos.

```bash
ghci> [1, 2, "Three"]

<interactive>:1:2: error:
    * No instance for (Num [Char]) arising from the literal `1'
    * In the expression: 1
      In the expression: [1, 2, "Three"]
      In an equation for `it': it = [1, 2, "Three"]
```

Se pueden crear series de elementos utilizando la *notación de enumeración*. Esta notación solo puede ser utilizada para elementos que se puedan enumerar.

```bash
ghci> [1..10]
[1,2,3,4,5,6,7,8,9,10]
```

Podemos indicar el incremento a utilizar en nuestra enumeración indandolo mediante los dos primeros elementos en la misma, seguido del valor en el cuál se terminará.
```bash
ghci> [1,4..17]
[1,4,7,10,13,16]
```

Debido a que la enumeracón anterior presenta incrementos de 3, esta misma terminará antes de llegar al número 17.

En caso de no indicar el valor en el cual terminará nuestra enumeración [1..], se producirán valores indefinidamente, con lo cual tendremos que forzar la terminación del proceso actual.

### Operaciones con listas

Podemos realizar concatenación de listas mediante el operador **++**.

```bash
ghci> [1,2,3] ++ [4,5]
[1,2,3,4,5]
```

Mediante el operador **:** agregamos un elemento al inicio de una lista. Este operador siempre deberá ser precedido por un elemento y sucedido por una lista (1 : []), si intentamos realizarlo al revés con el fin de agregar el mismo elemento al final de la liste ([] : 1) obtendremos un error.

```bash
ghci> 1 : [2,3,4,5]
[1,2,3,4,5]
```