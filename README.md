

# [Aprende Haskell Practicando](https://alessandromilan.gitlab.io/aprende-haskell-practicando/)

## Índice

* [Aprende Haskell Practicando](/README.md)
* [Introducción](/introduccion/README.md)
* Comenzando
    * [Configurando Haskell](/configurando-haskell/README.md)
    * [Interacción básica](/interaccion-basica/README.md)
        * [Opreciones aritméticas simples](/interaccion-basica/aritmetica-simple/README.md)
        * [Lógica booleana](/interaccion-basica/logica-booleana/README.md)
        * [Precedencia de operadores y asociatividad](/interaccion-basica/precedencia-y-asociatividad/README.md)
        * [Uso de variables](/interaccion-basica/uso-de-variables/README.md)
    * [Listas](/listas/README.md)

AMilan 2018