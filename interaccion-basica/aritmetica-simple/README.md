### Opreciones aritméticas simples

Las operaciones aritméticas trabajan de una forma muy similar a los lenguajes de programación más comunes como C++, Java, C#, etc. Las operaciones son escirtas con notación infija, en donde un operador binario aparece entre dos operandos:

```bash
ghci>  2 + 2
4
ghci> 2 * 2
4
ghci> 9.0 / 2.0
4.5
```

Adicionalmente podemos escribir una expresión con notación prefija. Para hacer esto debemos encerrar el operador entre paréntesis:

```bash
ghci> (+) 2 2
4
```

#### Escribiendo números negativos

En haskell continuamente es necesario encerrar los números negativos entre paréntesis. Al escribir un número negativo el signo **"-"** funge como un operador unario por lo cual si nosotros escribimos "-5" estaríamos escribiendo un "5" y aplicando a este número el operador unario "-". El operador unario "-" no puede ser mezclado con oparadores infijos.

```bash
ghci> -5
-5
```

```bash
ghci> 2 + -5
-5

<interactive>:1:1: error:
    Precedence parsing error
        cannot mix '+' [infixl 6] and prefix '-' [infixl 6] in the same infix expression
```

Si queremos combinar el operador unario "-"  junto con un operador infijo debemos encerrar la expresión a la que aplicaremos el operador unario y posteriormente aplicar esto al operador infijo.

```bash
ghci> 2 + (-5)
-3
```

Supongamos que tenemos una función llamada "f" y ésta recibe como parámetro un número negativo (**f -3**), al no existir la necesidad de encapsular el número negativo entre paréntesis, podríamos obtener dos resultados distintos:
* Aplicar la función al número negativo
* Restar el valor del número negativo al resultado de la función

La mayoría del tiempo podremos omitir los espacios en blanco de las expresiones y Haskell las parseare como es esperado, aunque hay ocasiones en donde esto no ocurrira.

```bash
ghci> 2*3
6

ghci> 2*-3

<interactive>:1:2: error:
    * Variable not in scope: (*-) :: Integer -> Integer -> t
    * Perhaps you meant one of these:
        `*' (imported from Prelude), `-' (imported from Prelude),
        `*>' (imported from Prelude)
```