## Interacción básica

¡ghci puede funcionar como una calculadora! A continuación se decriben las características básicas a emplear.

* [Opreciones aritméticas simples](aritmetica-simple\README.md)
* [Lógica booleana](logica-booleana\README.md)
* [Precedencia de operadores y asociatividad](precedencia-y-asociatividad\README.md)
* [Uso de variables](uso-de-variables\README.md)
* [¿Cómo lidiar con la precedencia y la asociatividad?](lidiando-con-problemas\README.md)
