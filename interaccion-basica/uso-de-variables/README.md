### Uso de variables

La biblioteca estándar **Prelude** define varias constantes bien conocidas como **PI**.

```bash
ghci> pi
3.141592653589793
```

Intentemos con el numero exponencial:

```bash
ghci> e

<interactive>:2:1: error: Variable not in scope: e
```

Como podemos observar tenemos que definirlo nosotros mismos. Para esto emplearemos la función exponencial incluida en la biblioteca estándar y obtendremos el exponencial del número 1, el valor obtenido de esta función lo asignaremos a una nueva variable **e**.

```bash
ghci> let e = exp 1
ghci> e
2.718281828459045
```