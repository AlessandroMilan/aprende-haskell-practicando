### Precedencia de operadores y asociatividad

La precedencia de operadores y la asociatividad en Haskell es tratada de manera similar a como se hace en álgebra. Del mismo modo podemos agrupar las expresiones entre paréntesis y omitir aquellos no necesarios.

```bash
ghci> 1 + (4 * 4)
17
ghci> 1 + 4 * 4
17
ghci> 1 + 4 * 4 + 1
18
```

Haskell asigna precedencia númerica a los operadores, siendo 1 la menor precedencia y 9 la mayor. Aquellos operadores con mayor precedencia son tomados en cuenta primero. Mediante el comando **:info** podemos inspeccionar la precedencia de cada operador.

```bash
ghci>:info (+)
class Num a where
  (+) :: a -> a -> a
  ...
        -- Defined in 'GHC.Num'
infixl 6 +
ghci>:info (-)
class Num a where
  ...
  (-) :: a -> a -> a
  ...
        -- Defined in 'GHC.Num'
infixl 6 -
ghci>:info (*)
class Num a where
  ...
  (*) :: a -> a -> a
  ...
        -- Defined in 'GHC.Num'
infixl 7 *
```

Mediante la palabra **infixl** detectamos que los operadores anteriores son asociativos por la izquierda y el número que le sucede es la precedencia de cada operador.