### Lógica booleana

La valores booleanos en Haskell son **True** y **False** y los operadores empleados son **&&** (AND lógico) y **||** (OR ógico).

```bash
ghci> True && False
False
ghci> True || False
True
```

En Haskell ni el número 0 es considerado **False** ni los números distintos a 0 son consideraros **True**.

```bash
ghci> True && 1

<interactive>:1:9: error:
    * No instance for (Num Bool) arising from the literal `1'
    * In the second argument of `(&&)', namely `1'
      In the expression: True && 1
      In an equation for `it': it = True && 1
```

El error arrojado nos indica que el el valor númerico **1** no puede ser tratado como un Booleano.

#### Comparar valores

La mayoría de los operadores de comparación en Haskell son similares a los manejados en otros lenguajes de programación.

```bash
ghci> 1 == 1
True
ghci> 2 < 3
True
ghci> 2 > 3
False
ghci> 2 >= 3
False
```

A diferencia de otros lenguajes de programación, si queremos usar el operador **!=** (Distinto a) tendremos que hacerlo de la siguiente manera:

```bash
ghci> 1 /= 2
True
```

Adicionalmente en Haskell no existe el oprador lógico NOT, represantado en otros lenguajes mediante el signo de exclamación **!**, para realizar una negación lógica emplearemos la función **not**.

```bash
ghci> not False
True
```