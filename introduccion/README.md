# Introducción

A lo largo de este libro aprenderemos Haskell, un lenguaje de programación funcional que se veulve cada vez más popular. Entenderemos como usar Haskell aplicado a problemas reales.

## Al terminar este libro seremos capaces de:
* Entender que es la programación funcional
* Desarrollar programas útiles implementando las características que Haskell nos provee
* Interactuar con sistemas de archivos, bases de datos y servicios en red
* Escribir código solido con pruebas automatizadas, covertura de código y manejo de errores
* Aprovechar el poder de los sistemas multi-núcleo a través de programación concurrente y paralela