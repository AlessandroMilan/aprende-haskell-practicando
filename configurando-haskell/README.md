# Configurando Haskell

En este libro utilizaremos GHC (Glasgow Haskell Compiler). GHC realiza una compilación de tu código fuente a código nativo, soporta execución en paralelo y provee de herramientas de análisis de rendimiento y depuración. GHC se compone de tres partes:

* **ghc** Un compilador optimizador que genera código nativo rápido.

* **ghci** Un interprete interactivo y depurador

* **runghc** Un programa con el cual correras programas escritos en Haskell como si fueran scripts, no hay necesidad de compilar tus programas primero.

## Instalación

Recomiendo instalar [Haskell Platform](https://www.haskell.org/platform/). Dependiendo del sistema operativo que se use es el tipo de instalador a emplear. En este libro no se cubrirá a detalle la instalación.

Una vez instalado Haskell Platform procedemos a utilizar ***ghci***. Para realizar esto abriremos una nueva ventana en terminal y ejecuteramos el comando ***ghci***, una vez hecho esto entraremos al interprete de Haskell.
```bash
GHCI, version 8.4.3: http://www.haskell.org/ghc/    :? for help
Prelude>
```

La palabra **Prelude>** en la entrada indica que **Prelude**, la biblioteca estándar ha sido cargada y está lista para utilizarse. Cuando carguemos otros módulos o archivos fuente también seran mostrados en la entrada.

La palabra mostrada en la entrada de la terminal cambia constantemente de acuerdo a los módulos que se encuentren cargados, icrementando lo suficiente como para no dejar espacio en la línea de entrada de texto.

Para fines prácticos sustituiremos el texto a mostrar en la entrada por la palabra "ghci"

```bash
Prelude> :set prompt "ghci>"
```
